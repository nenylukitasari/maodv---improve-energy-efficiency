# MAODV - Improve Energy Efficiency

Nama&nbsp;&nbsp;&nbsp;: Neny Lukitasari <br/>
NRP&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  : 05111540000018<br/>
Kelas&nbsp;&nbsp;&nbsp;&nbsp;: A<br/>
Details     : Modifikasi yang dilakukan ditujukan untuk mengoptimalkan efisiensi dari energy yang digunakan, lebih tepatnya residual energy yang terpakai.
Berikut adalah beberapa modifikasi yang dilakukan dari protokol AODV asli:
              <ol>
                <li>Membuat field baru pada paket RREQ dan RREP untuk menampung energy values.</li>
                <li>Implementasi fungsi baru untuk menghitung energi residu dari sebuah node.</li>
                <li>Membuat energy vector untuk proses penyeleksian pencarian best path.</li>
                

